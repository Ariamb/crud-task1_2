require('dotenv').config()
const express = require('express')
//const mongoose = require('mongoose')

//
const app = express()
app.use(express.json())

//db
//mongoose.connect(process.env.MONGO_DB_POSTS, { useUnifiedTopology: true, useNewUrlParser: true })
require('./src/Models/Post')
require('./src/Models/User')
//.const Post = mongoose.model('Post')


app.use('/', require('./src/routes'))

app.listen(3001)
