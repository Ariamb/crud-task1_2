const express = require('express')
var router = express.Router()
const postController = require('./postController')
const userController = require('./userController')
router.get('/post/search',  postController.search)
router.get('/post/search/:id', postController.searchOne)
router.post('/post/create', postController.store)
router.put('/post/update/:id', postController.update)
router.delete('/post/delete/:id', postController.destroy)

router.get('/user/search', userController.search)
router.get('/user/search/:id', userController.searchOne)
router.post('/user/create', userController.store)
router.put('/user/update/:id', userController.update)
router.delete('/user/delete/:id', userController.destroy)




module.exports = router