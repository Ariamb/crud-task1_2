mongoose = require('mongoose')
mongoose.connect(process.env.MONGO_DB_POSTS, { useUnifiedTopology: true, useNewUrlParser: true })
mongoose.set('useFindAndModify', false);
const post = mongoose.model("Post")

module.exports = { //search, store, update, delete
    async search(req, res) {
        const posts = await post.find()
        return res.json(posts)
    },
    async searchOne(req, res) {
        const posts = await post.findById(req.params.id)
        return res.json(posts)
    },
    async store(req, res) {
        const posts = await post.create(req.body)
        return res.json(posts)
    },
    async update(req, res) {
        const posts = await post.findByIdAndUpdate(req.params.id, req.body, { new: true })
        return res.json(posts)
    },
    async destroy(req, res) {
        await post.findByIdAndDelete(req.params.id)
        return res.send()
    }

}