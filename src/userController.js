mongoose = require('mongoose')
mongoose.connect(process.env.MONGO_DB_USERS, { useUnifiedTopology: true, useNewUrlParser: true })
mongoose.set('useFindAndModify', false);
const user = mongoose.model('User')
const bcrypt = require('bcrypt')

function hashPassword(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5))
}
module.exports = { //search, store, update, delete
    async search(req, res) {
        const users = await user.find()
        return res.json(users)
    },
    async searchOne(req, res) {
        const users = await user.findById(req.params.id)
        return res.json(users)
    },
    async store(req, res) {
        var salt = bcrypt.genSaltSync(5)
        req.body.password = hashPassword(req.body.password)
        const users = await user.create(req.body)
        return res.json(users)
    },
    async update(req, res) {
        req.body.password = hashPassword(req.body.password)
        const users = await user.findByIdAndUpdate(req.params.id, req.body, { new: true })
        return res.json(users)
    },
    async destroy(req, res) {
        await user.findByIdAndDelete(req.params.id)
        return res.send()
    }

}