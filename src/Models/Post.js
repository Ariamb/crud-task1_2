const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    body: {
        type: String,
        required: true,
    },
    likes: {
        type: Number,
        required: false,
    },
    autor: {
        type: String,
        required: true,
    },
})
mongoose.model('Post', schema)